let express = require('express')
let app = express()
const DATA = require("./data/data.json");
console.clear();

app.get('/spaces', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Content-Type', 'application/json');

    res.json(DATA.spaces)
    res.status(200);
})

app.get('/spaces/:id', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Content-Type', 'application/json');

    let space = DATA.spaces.find(v => Number(v.id) === Number(req.params.id));
    space === undefined ? res.json("error") : res.json(space);
    res.status(200);
})

app.get('/packages', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Content-Type', 'application/json');

    res.json(DATA.packages)
    res.status(200);
})

app.get('/packages/:id', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Content-Type', 'application/json');

    let package = DATA.packages.find(v => Number(v.id) == req.params.id);
    package === undefined ? res.json("error") : res.json(package);
    res.status(200);
})

app.post('/reservation', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Content-Type', 'application/json');

    let reservation = {name: "hi"};
    console.log(reservation);
    res.status(200);
})

app.listen(7777)